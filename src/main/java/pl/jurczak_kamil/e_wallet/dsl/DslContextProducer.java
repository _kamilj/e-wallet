package pl.jurczak_kamil.e_wallet.dsl;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import javax.enterprise.context.ApplicationScoped;
import java.sql.DriverManager;
import java.sql.SQLException;

@ApplicationScoped
public class DslContextProducer {

    public DSLContext getDSLContext() {
        try {
            final var url = "jdbc:postgresql://localhost:5432/e_wallet";
            final var username = "postgres";
            final var password = "postgres";

            DriverManager.registerDriver(new org.postgresql.Driver());
            final var connection = DriverManager.getConnection(url, username, password);
            if (connection != null) {
                System.out.println("Connected to the database!");
            } else {
                System.out.println("Failed to make connection!");
            }
            return DSL.using(connection, SQLDialect.POSTGRES);
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
