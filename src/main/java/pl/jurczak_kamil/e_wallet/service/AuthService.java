package pl.jurczak_kamil.e_wallet.service;

import pl.jurczak_kamil.e_wallet.dsl.DslContextProducer;
import pl.jurczak_kamil.e_wallet.model.jooq.Tables;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class AuthService {

    @Inject
    DslContextProducer dslContextProducer;

    public boolean checkLogin(String username, String password) {
        return dslContextProducer.getDSLContext()
                .selectCount()
                .from(Tables.SYS_AUTH)
                .where(Tables.SYS_AUTH.USERNAME.equals(username))
                .and(Tables.SYS_AUTH.PASSWORD.equals(password))
                .fetch()
                .isNotEmpty();
    }
}
