package pl.jurczak_kamil.e_wallet.view;


import lombok.Getter;
import lombok.Setter;
import org.primefaces.PrimeFaces;
import pl.jurczak_kamil.e_wallet.service.AuthService;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Getter
@Setter
@Named
@RequestScoped
public class AuthView implements Serializable {

    @Inject
    private AuthService authService;

    private String username;
    private String password;

    public void login() {
        FacesMessage message = null;
        final var loggedIn = authService.checkLogin(username, password);
        if(loggedIn) {
            message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Welcome", username);
        } else {
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Loggin Error", "Invalid credentials");
        }

        FacesContext.getCurrentInstance().addMessage(null, message);
        PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggedIn);
    }
}
